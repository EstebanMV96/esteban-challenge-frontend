export class Response {

    servers?: Server[];
    servers_changed?: boolean;
    ssl_grade?: string;
    previous_ssl_grade?: string;
    logo?: string;
    title?: string;
    is_down?: boolean;
    
    constructor(){
        this.servers = [];
        this.ssl_grade = "no info";
        this.previous_ssl_grade = "no info";
        this.logo = "no info";
        this.title = "no info";
    }
   
}

export class Server {
    address?: string
    ssl_grade?: string
    country?: string
    owner?: string
}
